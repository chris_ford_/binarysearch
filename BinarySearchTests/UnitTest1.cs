﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using BinarySearchKata;
using NUnit.Framework;

namespace BinarySearchTests
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        [TestCase(-1, 3, "")]
        [TestCase(-1, 3, "1")]
        [TestCase(-0, 1, "1")]
        [TestCase(0, 1, "1, 3, 5")]
        [TestCase(1, 3, "1, 3, 5")]
        [TestCase(2, 5, "1, 3, 5")]
        [TestCase(-1, 0, "1, 3, 5")]
        [TestCase(-1, 2, "1, 3, 5")]
        [TestCase(-1, 4, "1, 3, 5")]
        [TestCase(-1, 6, "1, 3, 5")]
        [TestCase(0, 1, "1, 3, 5, 7")]
        [TestCase(1, 3, "1, 3, 5, 7")]
        [TestCase(2, 5, "1, 3, 5, 7")]
        [TestCase(3, 7, "1, 3, 5, 7")]
        [TestCase(-1, 0, "1, 3, 5, 7")]
        [TestCase(-1, 2, "1, 3, 5, 7")]
        [TestCase(-1, 4, "1, 3, 5, 7")]
        [TestCase(-1, 6, "1, 3, 5, 7")]
        [TestCase(-1, 8, "1, 3, 5, 7")]
        public void TestSearch(int expectedResult, int searchItem, string arrayString)
        {

            int[] array;
            if (string.IsNullOrWhiteSpace(arrayString))
            {
                array = new int[] { };
            }
            else
            {
                array = Array.ConvertAll(arrayString.Split(','), x => int.Parse(x));
            }

            var binarySearch = new BinarySearch();
            var result = binarySearch.DoSearch(array.ToList(), searchItem);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
