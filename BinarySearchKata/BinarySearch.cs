﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchKata
{
    public class BinarySearch
    {
        private bool isItemFound = false;
        private int indexOfItemFound;
        private int indexOfGlobalArray;
        private List<int> splitList = new List<int>();
        private bool arrayIsEmpty;

        public int DoSearch(List<int> array, int searchItem)
        {
            splitList = array;
            while (!isItemFound && !arrayIsEmpty)
            {
                CheckItemDirection(splitList, searchItem);
            }

            if (!isItemFound && arrayIsEmpty)
            {
                indexOfGlobalArray = -1;
            }

            return indexOfGlobalArray;
        }

        private void CheckItemDirection(List<int> array, int searchItem)
        {
            if (array.Any())
            {
                decimal lengthMidPoint = array.Count/2;

                var roundedMidPoint = (int) Math.Round(lengthMidPoint, 0, MidpointRounding.AwayFromZero);

                if (array[roundedMidPoint] == searchItem)
                {
                    indexOfItemFound = indexOfGlobalArray + roundedMidPoint;
                    indexOfGlobalArray = indexOfItemFound;
                    isItemFound = true;
                }
                else
                {
                    if (array.Count == 1 && !isItemFound)
                    {
                        isItemFound = false;
                        arrayIsEmpty = true;
                    }
                    else
                    {
                        if (searchItem < array[roundedMidPoint])
                        {
                            splitList = array.Take(roundedMidPoint).ToList();
                            if (!splitList.Any())
                            {
                                arrayIsEmpty = true;
                            }
                        }
                        else
                        {
                            splitList = array.Skip(roundedMidPoint).ToList();
                            indexOfGlobalArray = indexOfGlobalArray + roundedMidPoint;
                        }
                    }
                }
            }
            else
            {
                arrayIsEmpty = true;
            }
        }
    }
}
